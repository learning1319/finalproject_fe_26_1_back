const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");

const CustomerSchema = new Schema(
    {
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        enabled: {
            type: Boolean,
            required: true,
            default: true
        },
        cart: {
            type: Array,
            required: true,
            default: []
        },
        wishlist: {
            type: Array,
            required: true,
            default: []
        },
        phoneNumber: {
            type: String,
            required: false,
        },
        birthDate: {
            type: String,
            required: false
        }
    },
    { strict: false }
);

CustomerSchema.methods.comparePassword = async function(candidatePassword) {
    try {
        console.log(this.password + " and here we stopped");
        return await bcrypt.compare(candidatePassword, this.password);
    } catch (err) {
        throw new Error('Error comparing passwords');
    }
};

module.exports = Customer = mongoose.model("customers", CustomerSchema);
