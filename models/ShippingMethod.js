const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ShippingMethodSchema = new Schema(
  {
    userId: {
      type: String,
      required: true
    },
      shippingDetails: {
        // by courier
          courierName: {
              type: String,
              trim: true,
          },
          trackingNumber: {
              type: String,
              trim: true,
          },
          deliveryAddress: {  // тут может быть адрес пользователя из БД, либо из редакса
              type: String,
              trim: true,
          },
          contactNumber: {
              type: String,
              trim: true,
          },

          // by mail
          mailService: {
              type: String,
              trim: true,
          },
          mailTrackingNumber: {
              type: String,
              trim: true,
          },
          mailDeliveryAddress: {
              type: String,
              trim: true,
          },
          deliveryOptions: {
              type: String,
              enum: ['Standard', 'Express', 'Registered'],
          },

          // by pickup
          pickupLocation: {
              type: String,
              trim: true,
          },
          pickupTime: {
              type: String,
              trim: true,
          },
          pickupReferenceNumber: {
              type: String,
              trim: true,
          },
      },
    phone: {
        type: String,
        required: true
    },
    name: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    enabled: {
      type: Boolean,
      required: true,
      default: true
    },
    default: {
      type: Boolean,
      required: true,
      default: false
    },
    deliveryTime: {
      type: String
    },
    currency: {
      type: String
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  { strict: false }
);

module.exports = ShippingMethod = mongoose.model(
  "shipping-methods",
  ShippingMethodSchema,
  "shipping-methods"
);
