const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema(
  {
    itemNo: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    products: {
        type: Array,
      required: true,
    },
    price: {
        type: Number,
        required: true
    },
    fabric: {
      type: String,
      required: true
    },
    category: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true
    },
    new: {
      type: Boolean,
      required: true,
      default: false
    },
    plusSize: {
      type: Boolean,
      required: true,
      default: false,
    },
    favorite: {
      type: Boolean,
      required: false
    },
    imageUrls: [
      {
        type: String,
        required: true
      }
    ],
    quantity: {
      type: Number,
      required: true,
      default: 0
    },
    color: {
      type: String
    },
    sizes: {
      type: String
    },
    productUrl: {
      type: String
    },
    brand: {
      type: String
    },
    manufacturer: {
      type: String
    },
    manufacturerCountry: {
      type: String
    },
    seller: {
      type: String
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  { strict: false }
);

ProductSchema.index({ "$**": "text" });

module.exports = Product = mongoose.model("products", ProductSchema);
