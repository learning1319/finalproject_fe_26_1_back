const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentSchema = new Schema(
  {
    userId: {
        type: String,
        required: true
    },
    customerName: {
        type: String,
        required: true
    },
    customerSurname: {
      type: String,
      required: true
    },
    productId: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    rate: {
        type: Number,
        required: true
    }
  },
);

module.exports = Comment = mongoose.model("comments", CommentSchema);
