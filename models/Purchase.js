const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const purchaseSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    shippingDetails: {
        type: Schema.Types.Mixed,
        required: true,
    },
    paymentDetails: {
        type: Schema.Types.Mixed,
        required: true,
    },
    products: {
        type: Array,
        required: true,
    },
    status: {
        type: String,
    },
    totalPrice: {
        type: Number,
        required: true
    },
    phone: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Purchase = mongoose.model("purchases", purchaseSchema)