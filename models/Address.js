const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AddressSchema = new Schema({
    userId: {
      type: String,
      required: true
    },
    email: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    country: {
        type: String,
    },
    city: {
        type: String,
        required: true
    },
    company: {
        type: String,
        trim: true
    },
    addressLine1: {
        type: String,
        required: true
    },
    addressLine2: {
        type: String,
        required: true
    },
    zip: {
        type: String,
        required: true
    },
    phone: {
        type: String,
    },
    default: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("Address", AddressSchema);