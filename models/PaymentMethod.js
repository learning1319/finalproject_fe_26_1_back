const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PaymentMethodSchema = new Schema(
  {
    userId: {
      type: String,
      required: true
    },
      paymentDetails: {
        // by card
          cardNumber: {
              type: String,
              trim: true,
          },
          cardExpiryMonth: {
              type: String,
          },
          cardExpiryYear: {
              type: String
          },
          cardCVV: {
              type: String,
              trim: true,
          },

          // by bank TRANSFER
          bankName: {
              type: String,
              trim: true,
          },
          routingNumber: {
              type: String,
              trim: true,
          },
          accountNumber: {
              type: String,
              trim: true,
          },
          accountHolderName: {
              type: String,
              trim: true,
          },

          // by online banking
          onlineBankingProvider: {
              type: String,
              trim: true,
          },
          onlineBankingUsername: {
              type: String,
              trim: true,
          },

          // by cash
          cashLocation: {
              type: String,
              trim: true,
          },
          cashPaymentReference: {
              type: String,
              trim: true,
          },

          // by wallet
          walletEmail: {
              type: String,
              trim: true,
          },

          // by crypto
          cryptoAddress: {
              type: String,
              trim: true,
          },
      },
    enabled: {
      type: Boolean,
      required: true,
      default: true
    },
    default: {
      type: Boolean,
      required: true,
      default: false
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  { strict: false }
);

module.exports = PaymentMethod = mongoose.model(
  "payment-methods",
  PaymentMethodSchema,
  "payment-methods"
);