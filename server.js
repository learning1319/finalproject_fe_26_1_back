require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const path = require('path');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const globalConfigs = require('./routes/globalConfigs');
const customers = require('./routes/customers');
const contacts = require('./routes/contact')
const products = require('./routes/products');
const colors = require('./routes/colors');
const sizes = require('./routes/sizes');
const filters = require('./routes/filters');
const subscribers = require('./routes/subscribers');
const cart = require('./routes/cart');
const orders = require('./routes/orders');
const links = require('./routes/links');
const pages = require('./routes/pages');
const slides = require('./routes/slides');
const wishlist = require('./routes/wishlist');
const comments = require('./routes/comments');
const shippingMethods = require('./routes/shippingMethods');
const paymentMethods = require('./routes/paymentMethods');
const partners = require('./routes/partners');
const bannerRoutes = require('./routes/bannerRoutes');
const purchaseRoutes = require('./routes/purchase');
const addressRoutes = require('./routes/address');
const authRoutes = require('./routes/googleCustomer');
const cookiesRoutes = require('./routes/cookieRoutes');

if (!process.env.MONGO_URI) {
  throw new Error('MONGO_URI is not defined in the .env file');
}
const app = express();
app.use(cors({
  origin: ['http://localhost:3000', 'https://modimalshop.vercel.app', 'https://finalproject-fe-26-1-front-oa6zk6xzf-yaroslavaszukhas-projects.vercel.app', 'https://modimal-5rsmf1ls3-yaroslavazasukhas-projects.vercel.app'],
  credentials: true,
}))
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose
    .connect(process.env.MONGO_URI, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true })
    .then(() => console.log('MongoDB Connected'))
    .catch((err) => console.log(err));

app.use(passport.initialize());
require('./services/passport')(passport);

// Use Routes
app.use('/api/configs', globalConfigs);
app.use('/api/customers', customers);
app.use('/api/contacts', contacts);
app.use('/api/products', products);
app.use('/api/colors', colors);
app.use('/api/sizes', sizes);
app.use('/api/filters', filters);
app.use('/api/subscribers', subscribers);
app.use('/api/cart', cart);
app.use('/api/orders', orders);
app.use('/api/links', links);
app.use('/api/pages', pages);
app.use('/api/slides', slides);
app.use('/api/wishlist', wishlist);
app.use('/api/comments', comments);
app.use('/api/shipping-methods', shippingMethods);
app.use('/api/partners', partners);
app.use('/api/banners', bannerRoutes);
app.use('/api/payment-methods', paymentMethods);
app.use('/api/purchase', purchaseRoutes);
app.use('/api/address', addressRoutes);
app.use('/api/cookies', cookiesRoutes);
app.use('/auth', authRoutes);

const port = process.env.NODE_ENV === 'production' ? process.env.PORT : 4000;
app.listen(port, () => console.log(`Server running on port ${port}`));