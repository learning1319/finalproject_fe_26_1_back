const express = require("express");
const {addPurchase, getUserPurchases, updatePurchase, getPurchases} = require("../controllers/purchase");
const passport = require("passport");
const router = express.Router();

// passport

// adding purchase
router.post(
    '/',
    passport.authenticate("jwt", { session: false }),
    addPurchase
);

// getting all purchases
router.get(
    '/:userId',
    passport.authenticate("jwt", { session: false }),
    getUserPurchases
);


// Route for changing product status
router.put(
    '/',
    passport.authenticate("jwt-admin", { session: false }),
    updatePurchase
)

// Route for getting all purchases
router.get(
    '/',
    passport.authenticate("jwt-admin", { session: false }),
    getPurchases
)

module.exports = router;