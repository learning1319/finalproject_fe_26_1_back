const express = require("express");
const router = express.Router();
const passport = require("passport");

//Import controllers
const {
  addContact,
  getContacts
} = require("../controllers/contact");

// Route for adding contacts
router.post(
    '/',
    addContact
);

// Route for getting all contacts
router.get(
    '/',
    passport.authenticate("jwt-admin", { session: false }),
    getContacts
)

module.exports = router;
