const express = require("express");
const router = express.Router();
const passport = require("passport");

//Import controllers
const {
  addDefaultPaymentMethod,
  updatePaymentMethod,
  deletePaymentMethod,
  getPaymentMethods,
  getUserMethod,
    addPaymentMethod
} = require("../controllers/paymentMethods");

// passport

// @route   POST /payment-methods
// @desc    Create new payment method
// @access  Private
router.post(
    "/default",
    passport.authenticate("jwt", { session: false }),
    addDefaultPaymentMethod
);

router.post(
    '/',
    passport.authenticate("jwt", { session: false }),
    addPaymentMethod
)

// @route   PUT /payment-methods/:customId
// @desc    Update existing payment method
// @access  Private
router.put(
    "/:customId",
    passport.authenticate("jwt", { session: false }),
    updatePaymentMethod
);

// @route   DELETE /payment-methods/:customId
// @desc    DELETE existing payment method
// @access  Private
router.delete(
    "/:customId",
    passport.authenticate("jwt", { session: false }),
    deletePaymentMethod
);

// @route   GET /payment-methods
// @desc    GET existing payment methods
// @access  Public
router.get(
    "/",
    passport.authenticate("jwt", { session: false }),
    getPaymentMethods
);

router.get(
    '/default',
    passport.authenticate("jwt", { session: false}),
    getUserMethod
)

module.exports = router;