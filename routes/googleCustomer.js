const express = require('express');
const router = express.Router();
const {startGoogleAuthorization, registerGoogleUser} = require('../controllers/googleCustomer');

// Route for starting Google authorization
router.get('/google', startGoogleAuthorization);

// Route for saving authorized user info
router.get('/google/callback', registerGoogleUser);

module.exports = router;