const express = require("express");
const router = express.Router();
const {
    getAddressById,
    deleteAddress,
    addAddress,
    getAddresses,
    updateAddress,
    getDefaultAddress
} = require('../controllers/address');
const passport = require("passport");

// passport
// Route for adding an address
router.post(
    '/',
    passport.authenticate("jwt", { session: false }),
    addAddress
);

// Route for getting all addresses
router.get(
    '/',
    passport.authenticate("jwt", { session: false }),
    getAddresses
);

// Route for getting address with default value
router.get(
    '/default',
    passport.authenticate("jwt", { session: false }),
    getDefaultAddress
)

// Route for updating address
router.put(
    '/',
    passport.authenticate("jwt", { session: false }),
    updateAddress
);

// Route for getting an exact address by ID
router.get(
    '/:id',
    passport.authenticate("jwt", { session: false }),
    getAddressById
);

// Route for deleting an exact address by ID
router.delete(
    '/:id',
    passport.authenticate("jwt", { session: false }),
    deleteAddress
);

module.exports = router;