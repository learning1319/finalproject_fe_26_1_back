const express = require('express');
const router = express.Router();

//Import controllers
const {
    getBanners,
    createBanner,
    getBannersById,
    updateIdBanner
  } = require("../controllers/bannerController");

// Route for getting all banners
router.get("/", getBanners);

// Route for getting banners by category and name
router.get('/:category/:name', getBannersById)

// Route for getting banners by categor field
router.get('/:category', getBannersById);

// Route for getting banners by name
router.get('/name/:name', getBannersById);

// Route for getting products by category in array
router.get('/category/:categoryName', getBannersById);

// Route for getting product by name and changing ids inside
router.post('/name/:name', updateIdBanner)

// Route for creating banner
router.post('/', createBanner);

module.exports = router;