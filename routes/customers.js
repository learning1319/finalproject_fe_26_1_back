const express = require("express");
const router = express.Router();
const passport = require("passport");

//Import controllers
const {
  createCustomer,
  loginCustomer,
  getCustomer,
  editCustomerInfo,
  addToWishlist,
  removeFromWishlist,
  updatePassword,
  deleteCustomer,
  addToCart,
  removeFromCart,
  clearWishList,
  clearCart
} = require("../controllers/customers");

// @route   POST /customers
// @desc    Register customer
// @access  Public
router.post("/", createCustomer);

// @route   POST /customers/login
// @desc    Login Customer / Returning JWT Token
// @access  Public
router.post("/login", loginCustomer);

// @route   GET /
// @desc    Return current customer
// @access  Private
router.get(
  "/customer",
  passport.authenticate("jwt", { session: false }),
  getCustomer
);

// @route   PUT /customers
// @desc    Return current customer
// @access  Private
router.put(
  "/",
  passport.authenticate("jwt", { session: false }),
  editCustomerInfo
);

// @route   POST /customers/profile/update-password
// @desc    Return current customer and success or error message
// @access  Private
router.put(
  "/password",
  passport.authenticate("jwt", { session: false }),
  updatePassword
);

// @route   POST /customers/wishlist
// @desc    Add item to wishlist
// @access  Private
router.post(
  "/wishlist",
  passport.authenticate("jwt", { session: false }),
  addToWishlist
);

// Route for adding product to cart
router.post(
    '/cart',
    passport.authenticate('jwt', { session: false }),
    addToCart
);


// Route for deleting product from cart
router.delete(
    '/cart',
    passport.authenticate('jwt', { session: false }),
    removeFromCart
);

// @route   DELETE /customers/wishlist
// @desc    Remove item from wishlist
// @access  Private
router.delete(
  "/wishlist",
  passport.authenticate("jwt", { session: false }),
  removeFromWishlist
);

// Route for clearing users wishlist
router.delete(
    '/clear',
    passport.authenticate('jwt', { session: false }),
    clearWishList
)

// Route for clearing users cart
router.delete(
    '/clearCart',
    passport.authenticate('jwt', { session: false }),
    clearCart
)

// Route for deleting user from database
router.delete(
  '/delete-account',
  passport.authenticate("jwt", { session: false }),
  deleteCustomer
);

module.exports = router;
