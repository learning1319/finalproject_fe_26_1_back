const express = require('express');
const router = express.Router();
const {getFromCookies} = require('../controllers/googleCustomer');

// Router for getting information from cookies
router.get('/',  getFromCookies);

module.exports = router;