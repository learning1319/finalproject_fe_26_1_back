const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(10000000, 99999999);
const secret = process.env.SECRET_OR_KEY;
const Customer = require("../models/Customer");
const validateRegistrationForm = require("../validation/validationHelper");
const queryCreator = require("../commonHelpers/queryCreator");
const {ObjectId} = require("mongodb");

exports.createCustomer = (req, res, next) => {
    const initialQuery = _.cloneDeep(req.body);
    initialQuery.customerNo = rand();

    const { errors, isValid } = validateRegistrationForm(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }

    Customer.findOne({email: req.body.email })
        .then(customer => {
            if (customer) {
                if (customer.email === req.body.email) {
                    return res
                        .status(400)
                        .json({ message: `Email ${customer.email} already exists"`, success: false, });
                }
            }

            const newCustomer = new Customer(queryCreator(initialQuery));
            const newToken = jwt.sign({
                _id: newCustomer._id,
                firstName: newCustomer.firstName,
                lastName: newCustomer.lastName,
                isAdmin: newCustomer.isAdmin
            }, process.env.SECRET_OR_KEY, {expiresIn: 36000});

            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newCustomer.password, salt, (err, hash) => {
                    if (err) {
                        res
                            .status(400)
                            .json({ message: `Error happened on server: ${err}`, success: false, });
                        return;
                    }

                    newCustomer.password = hash;
                    newCustomer
                        .save()
                        .then(customer => res.json({
                            success: true,
                            customer: customer,
                            token: newToken
                        }))
                        .catch(err =>
                            res.status(400).json({
                                message: `Error happened on server: "${err}" `, success: false,
                            })
                        );
                });
            });
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.loginCustomer = async (req, res, next) => {

    const { errors, isValid } = validateRegistrationForm(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }

    const loginOrEmail = req.body.loginOrEmail;
    const password = req.body.password;

    Customer.findOne({
        $or: [{ email: loginOrEmail }, { login: loginOrEmail }]
    })
        .then(customer => {
            if (!customer) {
                errors.loginOrEmail = "Customer not found";
                return res.json({
                    success: false
                })
            }

            bcrypt.compare(password, customer.password).then(isMatch => {
                if (isMatch) {

                    const payload = {
                        _id: customer._id,
                        firstName: customer.firstName,
                        lastName: customer.lastName,
                        isAdmin: customer.isAdmin
                    };

                    jwt.sign(
                        payload,
                        process.env.SECRET_OR_KEY,
                        { expiresIn: 36000 },
                        (err, token) => {
                            res.json({
                                success: true,
                                token: "Bearer " + token,
                                user: {
                                    _id: customer._id,
                                    loginOrEmail: loginOrEmail,
                                    password: password,
                                }
                            });
                        }
                    );
                } else {
                    errors.password = "Password incorrect";
                    return res.json({success: false})
                }
            });
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getCustomer = (req, res) => {
    return res.json(req.user);
};

exports.editCustomerInfo = async (req, res) => {

    const { _id, email, firstName, lastName, birthDate, phoneNumber} = req.body;

    const { errors, isValid } = validateRegistrationForm(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }
    try {
        const customer = await Customer.findOne({
            _id: new ObjectId(`${_id}`)
        })

        const occupiedCustomer = await Customer.findOne({email: email, _id: {
            $ne: new ObjectId(`${_id}`)
        }});

        if (occupiedCustomer){
            return res.json({
                message: "This email is already occupied"
            })
        }

        if (firstName) customer.firstName = firstName;
        if (lastName) customer.lastName = lastName;
        if (birthDate) customer.birthDate = birthDate;
        if (phoneNumber) customer.phoneNumber = phoneNumber;
        if (email) customer.email = email;

        await customer.save().then(res => console.log(res));

        return res.json({
            message: "Customer data was updated successfully",
            customer: customer
        });

    } catch (err) {
        return res.status(500).json({ message: `Server error: "${err.message}"` });
    }
};

// Массив из айди продуктов.
exports.addToWishlist = async (req, res) => {

    const { _id, productId } = req.body;

    await Customer.find({
        _id: new ObjectId(`${_id}`)
    })
        .then(async customer => {
            let wishlist = customer[0]["wishlist"];
            if (wishlist.includes(productId)){
                return res.json({ message: "Product is already in wishlist"});
            } else wishlist.push(productId);

            await Customer.updateOne({_id: new ObjectId(`${_id}`)}, {
                $set: { wishlist: wishlist }
            }).then(() => {
                return res.json({
                    message: `Added product. User's new wishlist is ${wishlist}`
                })
            })

        }).catch(err => { return res.json({
            message: `Error happened adding product to wishlist`,
            error: err.message
        })})
};

// Массив из айди продуктов.
exports.removeFromWishlist = async (req, res) => {

    const { _id, productId } = req.body;
    try {

        const customer = await Customer.findOne({ _id: new ObjectId(_id) });

        if (!customer) {
            return res.status(404).json({ message: "Customer not found" });
        }

        const wishlist = customer.wishlist;
        const productIndex = wishlist.findIndex(item => item === productId);

        if (productIndex === -1) {
            return res.json({ message: "Product is not in the wishlist" });
        } else {
            wishlist.splice(productIndex, 1);
            await Customer.updateOne(
                { _id: new ObjectId(_id) },
                { $set: { wishlist: wishlist } }
            );

            return res.json({ message: "Product removed from wishlist", wishlist });
        }

    } catch (e){
            console.error(e);
            return res.status(500).json({message: "Error happened on server", error: e.message});
    }
};

exports.clearWishList = async (req, res) => {

    const {_id} = req.body;
    const customer = await Customer.findOne({_id: new ObjectId(_id)});
    if (!customer) {
        return res.status(404).json({message: "Customer not found"});
    }

    await Customer.updateOne(
        { _id: new ObjectId(_id) },
        { $set: { wishlist: [] } }
    );
    return res.json({
        message: "Wishlist was cleared"
    })
}

// adds id of product to cart
exports.addToCart = async (req, res) => {
    const {_id, productId} = req.body;

    await Customer.find({
        _id: new ObjectId(`${_id}`)
    })
        .then(async customer => {

            const cart = customer[0]["cart"];
            cart.push(productId);

            await Customer.updateOne({_id: new ObjectId(`${_id}`)}, {
                $set: {
                    cart: cart
                }
            }).then(response => {
                return res.json({
                    message: `Added product. User's new cart is ${cart}`
                })
            });
        }).catch(err => { return res.json({
            message: `Error happened adding product to cart`,
            error: err.message
        })})
}

// removes id of product from cart
exports.removeFromCart = async (req, res) => {

    const {_id, productId} = req.body;

        await Customer.find({ _id: new ObjectId(_id) }).then(async customer => {
            if (!customer) {
                return res.status(404).json({ message: "Customer not found" });
            }

            const cart = customer[0]["cart"];
            console.log(cart);
            const productIndex = cart.findIndex(item => item === productId.toString());

            if (productIndex === -1) {
                return res.json({ message: "Product is not in the cart" });
            } else {
                cart.splice(productIndex, 1);
                await Customer.updateOne(
                    { _id: new ObjectId(_id) },
                    { $set: { cart: cart } }
                );

                return res.json({ message: "Product removed from cart", cart });
            }

        })
}

exports.clearCart = async (req, res) => {

    const { _id } = req.body;
    const customer = await Customer.find({_id: new ObjectId(_id)});
    if (!customer){ return res.json({ message: "Customer not found"})}

    await Customer.updateOne(
        { _id: new ObjectId(_id) },
        { $set: { cart: [] } }
    );
    return res.json({
        message: "Cart is empty"
    });

}

exports.updatePassword = async (req, res) => {
    const { errors, isValid } = validateRegistrationForm(req.body);
    if (!isValid) { return res.status(400).json(errors); }
    const { _id, currentPassword, newPassword } = req.body;
    try {
        const customer = await Customer.findOne({
            _id: new ObjectId(_id)
        });
        if (!customer){
            return res.status(404).json({
                message: "Customer was not found"
            })
        }
        const isMatch = await customer.comparePassword(currentPassword);
        if (!isMatch){
            errors.password = "Current password does not match";
            return res.status(400).json(errors);
        }
        const salt = await bcrypt.genSalt(10);
        customer.password = await bcrypt.hash(newPassword, salt);
        await customer.save().then(response => {
            return res.status(200).json({
              user: {
                  _id: customer._id,
                  password: newPassword,
                  email: customer.email
              },
              newData: response
            })
        })
    } catch (err) {
        return res.status(500).json({ message: `Server error: ${err.message}` });
    }

};

exports.deleteCustomer = async (req, res) => {

    const { _id } = req.body;

    await Customer.findOne({
        _id: new ObjectId(_id)
    }).then(customer => {
        if (!customer){
            return res.status(400).json({
                message: "Customer not found"
            })
        }
        Customer.findOneAndDelete({
            _id: new ObjectId(_id)
        }).then(response => {
            return res.status(200).json({
                message: "Customer was deleted",
                customer: response
            })
        })
    })
}


exports.findById = async (_id) => {
    try {
       await Customer.find({ _id: new ObjectId(`${_id}`) })
            .then(res => {
                if (res){
                    return res;
                } else return null;
            });

    } catch (err) {
        console.error("Error finding customer: ", err);
    }
}