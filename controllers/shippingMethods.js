const ShippingMethod = require("../models/ShippingMethod");
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(10000000, 99999999);


// {userId, shippingDetails}

exports.addShippingMethod = (req, res) => {

    const {_id, shippingDetails} = req.body;
    const match = {userId: _id, shippingDetails: shippingDetails}

  ShippingMethod.findOne(match).then(
    shippingMethod => {

        if (shippingMethod) {
        return res.status(400).json({
          message: `Shipping Method already exists`
        });

      } else {

            const data = _.cloneDeep(req.body);
            data.shippingNo = rand();
        const newShippingMethod = new ShippingMethod(queryCreator(data));

        newShippingMethod
          .save()
          .then(shippingMethod => res.status(200).json(shippingMethod))
          .catch(err =>
            res.status(400).json({
              message: `Error happened adding shipping method`,
                error: err.message
            })
          );
      }
    }
  ).catch(e => {
      return res.json({
          message: `Error happened on server: "${e}" `
      })
  })
};

// { userId, shippingDetails, newShippingDetails }

exports.updateShippingMethod = (req, res) => {

    const {_id, shippingDetails, newShippingDetails} = req.body;
    const match = {userId: _id, shippingDetails: shippingDetails}

  ShippingMethod.findOne(match)
    .then(shippingMethod => {
      if (!shippingMethod) {

          return res.status(400).json({
          message: `Shipping Method was not added.`
        });

      }
      else {

        ShippingMethod.findOneAndUpdate(
          match,
          { shippingDetails: newShippingDetails },
          { new: true }
        )
          .then(shippingMethod => res.json(shippingMethod))
          .catch(err =>
            res.status(400).json({
              message: `Error happened updating shipping method`,
                error: err.message
            })
          );
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};


// { userId, shippingDetails }

exports.deleteShippingMethod = (req, res, next) => {

    const { _id, shippingDetails } = req.body;
    const match = { userId: _id, shippingDetails: shippingDetails }

  ShippingMethod.findOne(match).then(
    async shippingMethod => {
      if (!shippingMethod) {
        return res.status(400).json({
          message: `Shipping Method was not found.`
        });
      } else {

          ShippingMethod.findOneAndDelete(match)
              .then(resp => res.status(200).json({
                  message: 'Shipping method was deleted',
                  shippingDetails: resp
              }))
              .catch(e => res.status(400).json({
                  massage: `Error happened on server: ${e}`
              }))
      }
    }
  );
};

// {userId}

exports.getShippingMethods = (req, res, next) => {

    const { _id } = req.body;

  ShippingMethod.find({ userId: _id })
    .then(shippingMethods => res.status(200).json(shippingMethods))
    .catch(err =>
      res.status(400).json({
        message: `Error happened getting shipping methods`,
          error: err.message
      })
    );
};


// {userId, shippingDetails}

exports.getShippingMethodById = (req, res, next) => {

    const { _id, shippingDetails } = req.body;

  ShippingMethod.findOne({ userId: _id, shippingDetails: shippingDetails})
    .then(shippingMethod => res.status(200).json(shippingMethod))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};