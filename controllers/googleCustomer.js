const axios = require('axios');
const querystring = require('querystring');
const Customer = require('../models/Customer');
const bcrypt = require("bcryptjs");
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(10000000, 99999999);
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET_OR_KEY;


exports.startGoogleAuthorization = (req, res) => {

    const authUrl = `https://accounts.google.com/o/oauth2/v2/auth?${querystring.stringify({
        client_id: process.env.GOOGLE_CLIENT_ID,
        redirect_uri: process.env.GOOGLE_REDIRECT_URI,
        response_type: 'code',
        scope: 'profile email',
        access_type: 'offline',
    })}`;
    res.redirect(authUrl);
}

exports.registerGoogleUser = async (req, res) => {

    const code = req.query.code;

    try {
        const tokenResponse = await axios.post('https://oauth2.googleapis.com/token', querystring.stringify({
            code,
            client_id: process.env.GOOGLE_CLIENT_ID,
            client_secret: process.env.GOOGLE_CLIENT_SECRET,
            redirect_uri: process.env.GOOGLE_REDIRECT_URI,
            grant_type: 'authorization_code',
        }), {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        });

        const accessToken = tokenResponse.data.access_token;

        await axios.get('https://www.googleapis.com/oauth2/v2/userinfo', {
            headers: { Authorization: `Bearer ${accessToken}` },
        }).then(async  data => {
            const {
                given_name: name,
                family_name: lastName,
                email: email,
                id: password
            } = data.data;

            const existingCustomer = await Customer.findOne({ email });

            if (existingCustomer) {

                const newToken = jwt.sign({
                    _id: existingCustomer._id,
                    firstName: existingCustomer.firstName,
                    lastName: existingCustomer.lastName,
                    isAdmin: existingCustomer.isAdmin
                }, secret, { expiresIn: 36000});

                const token = `Bearer ${newToken}`

                const info = {
                    success: true,
                    user: {
                        _id: existingCustomer._id,
                        loginOrEmail: email,
                        password: password
                    },
                    token: token
                }

                res.cookie('user', info, { expiresIn: 600000, path: '/'})

                return res.redirect('http://localhost:3000/')
            }

            const newCustomer = new Customer({
                firstName: name,
                lastName: lastName,
                email: email,
                password: password,
            });

            const salt = await bcrypt.genSalt(10);
            newCustomer.password = await bcrypt.hash(newCustomer.password, salt);

            await newCustomer.save().then(newCustomer => {

                const newToken = jwt.sign({
                    _id: newCustomer._id,
                    firstName: newCustomer.firstName,
                    lastName: newCustomer.lastName,
                    isAdmin: newCustomer.isAdmin
                }, secret, {expiresIn: '1h'});

                const token = `Bearer ${newToken}`

                const newInfo = {
                    success: true,
                    user: {
                        _id: newCustomer._id,
                        loginOrEmail: email,
                        password: password
                    },
                    token: token
                }

                res.cookie('user', newInfo, { expiresIn: 600000, path: '/'})
                return res.redirect('http://localhost:3000/');

            })

        })
    } catch (error) {
        console.error(error);
        res.status(500).send('Authentication failed.');
    }

}

exports.getFromCookies = async (req, res) => {

         const info = await req.cookies.user;

        if (!info) {
            return res.json({ message: 'Cookie was not set' });
        }
        try {
            return res.json({
                success: true,
                user: {
                    _id: info.user._id,
                    loginOrEmail: info.user.loginOrEmail,
                    password: info.user.password
                },
                token: info.token
            });
        } catch (error) {
            res.status(401).json({ error: 'Invalid token' });
        }
}