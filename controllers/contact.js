const Contact = require('../models/Contact');

exports.addContact = async (req, res) => {
    const newContact = new Contact(req.body);
    newContact.save().then(() => {
        return res.json({
            message: "Contacts were added successfully",
            success: true
        })
    });
}

exports.getContacts = async (req, res) => {
    const products = await Contact.find()
    return res.json(products);
}