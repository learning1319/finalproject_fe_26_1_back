const Banner = require('../models/Banner');

exports.getBanners = async (req, res) => {
  try {
    const banners = await Banner.find({ active: true });
    res.json(banners);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

exports.getBannersById = async (req, res) => {
  if (req.params.category && req.params.name && req.params.category !== 'name') {
    const { category, name } = req.params;

    if (req.params.category === 'category') {
      const category = req.params.name;

      const pipeline = [
        {
          "$match": {
            "category.name": category
          }
        }
      ];

      const products = await Banner.aggregate(pipeline).exec();
      return res.json(products);
    }

    try {
      const banners = await Banner.find({ category, name, active: true });

      if (banners.length === 0) {
        return res.status(404).json({ message: 'No banners found for the specified category and name' });
      }

      return res.json(banners);
    } catch (error) {
      console.error('Error fetching banners:', error);
      return res.status(500).json({ message: 'Internal server error' });
    }

  } else if (req.params.category && !req.params.name && req.params.category !== 'name') {
    const { category } = req.params;

    try {
      const banners = await Banner.find({ category, active: true });

      if (banners.length === 0) {
        return res.status(404).json({ message: 'No banners found for the specified category and name' });
      }

      return res.json(banners);
    } catch (error) {
      console.error('Error fetching banners:', error);
      return res.status(500).json({ message: 'Internal server error' });
    }

  } else if (req.params.category === 'name') {
    const { name } = req.params;

    try {
      const banner = await Banner.findOne({ name });
      if (!banner) {
        return res.status(404).json({ message: 'Banner not found' });
      }
      res.json(banner);
    } catch (err) {
      res.status(500).json({ message: `Server error: ${err.message}` });
    }
  }
};

exports.createBanner = async (req, res) => {
  const { name, id, imageUrl } = req.body;

  try {
    const newBanner = new Banner({ name, id, imageUrl });
    await newBanner.save();
    res.status(201).json(newBanner);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

exports.updateIdBanner = async (req, res) => {
  const { oldIds, newIds, imageUrl } = req.body;
  const { name } = req.params;

  if (oldIds && newIds) {
    if (oldIds.length !== newIds.length) {
      return res.status(400).json({ message: 'oldIds and newIds must have the same length.' });
    }

    try {
      const pipeline = [
        {
          $match: {
            "category.name": name
          }
        },
        {
          $project: {
            id: 1,
            category: {
              $map: {
                input: "$category",
                as: "cat",
                in: {
                  id: {
                    $cond: {
                      if: { $in: ["$$cat.id", oldIds] },
                      then: { $arrayElemAt: [newIds, { $indexOfArray: [oldIds, "$$cat.id"] }] },
                      else: "$$cat.id"
                    }
                  },
                  name: "$$cat.name"
                }
              }
            }
          }
        }
      ];

      const transformedBanners = await Banner.aggregate(pipeline).exec();
      console.log("Transformed Banners:", transformedBanners);

      for (const banner of transformedBanners) {
        const updateData = { category: banner.category };

        if (imageUrl) {
          updateData.imageUrl = imageUrl;
        }

        const updatedBanner = await Banner.findOneAndUpdate(
            { id: banner.id },
            { $set: updateData },
            { new: true }
        );

        if (!updatedBanner) {
          console.log(`Banner with id ${banner.id} not found`);
        } else {
          console.log("Updated Banner:", updatedBanner);
        }
      }

      return res.json({
        message: "Banners were updated",
        updatedBanners: transformedBanners
      });

    } catch (err) {
      console.error('Error updating banners:', err);
      res.status(500).json({ message: 'Internal server error.' });
    }
  } else {
    res.status(400).json({ message: 'oldIds and newIds are required.' });
  }
};
