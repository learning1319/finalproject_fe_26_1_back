const Comment = require("../models/Comment");
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");

// add comment
exports.addComment = async (req, res) => {
  const data = req.body;
  const newComment = await new Comment(data);
  newComment
    .save()
    .then(comment => res.json(comment))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

// Logged user can update comment.
exports.updateComment = async (req, res) => {
    const { userId, productId, newComment } = req.body;

    try {
        // Use `findOne` to get a single comment
        const comment = await Comment.findOne({
            userId: userId,
            productId: productId
        });

        if (!comment) {
            return res.status(400).json({
                message: `Comment was not found`
            });
        }

        // Update the comment's content and rate if provided
        const { content, rate } = newComment;
        if (content) { comment.content = content; }
        if (rate) { comment.rate = rate; }

        // Save the updated comment
        const updatedComment = await comment.save();

        return res.json({
            message: `Comment was successfully updated`,
            updatedComment: updatedComment
        });

    } catch (err) {
        console.error('Error updating comment:', err);
        res.status(500).json({
            message: 'Internal server error.'
        });
    }
};


exports.deleteComment = async (req, res) => {

    const { userId, productId } = req.query;

  await Comment.findOne({ userId: userId, productId: productId })
      .then(async comment => {
            if (!comment){
                return res.json({
                    message: `Comment was not found`
                })
            }
            if (comment){
                comment.delete().then(result => {
                    return res.json({
                        message: "Comment was deleted",
                        deletedComment: result
                    })
                })
            }
  });
};

exports.getComments = async (req, res) => {
  await Comment.find()
    .then(comments => res.json(comments))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.getCustomerComments = async (req, res) => {

    const { userId } = req.query;
    await Comment.find({ userId: userId })
    .then(comments => res.json(comments))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );

};


// returns comments of 1 product
exports.getProductComments = async (req, res) => {

  const {productId} = req.query;

  console.log(productId);
  await Comment.find({ productId: productId })
    .then(comments => res.json(comments))
    .catch(err =>
      res.json({
        message: `Error happened on server: "${err}" `
      })
    );
};
