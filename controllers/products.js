const Product = require("../models/Product");
const isValidMongoId = require("../validation/isValidMongoId");
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 999999);
const queryCreator = require("../commonHelpers/queryCreator");
const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");

exports.addImages = (req, res, next) => {
  if (req.files.length > 0) {
    res.json({
      message: "Photos are received"
    });
  } else {
    res.json({
      message:
          "Something wrong with receiving photos at server. Please, check the path folder"
    });
  }
};

exports.getProductsByString = async (req, res) => {

  const text = req.body.text;

  const pipeline = [{
    $match: {
      $or: [
        { description: { $regex: text, $options: "i" } },
        { name: { $regex: text, $options: "i" } }
      ]
    }
  }]

  try {

    const products = await Product.aggregate(pipeline).exec();
    return res.json(products);

  } catch (e) {
    return res.json({
      message: `Error happened searching for products`,
      error: e.message
    });
  }

}

// adds product in db
exports.addProduct = (req, res) => {

  const productFields = _.cloneDeep(req.body);

  productFields.itemNo = rand();

  try {
    productFields.name = productFields.name
        .toLowerCase()
        .trim()
        .replace(/\s\s+/g, " ");
  } catch (err) {
    return res.status(400).json({
      message: `Error happened on server: "${err}"`
    });
  }

  const updatedProduct = queryCreator(productFields);

  const newProduct = new Product(updatedProduct);

  newProduct
      .save()
      .then(product => res.json(product))
      .catch(err =>
          res.status(400).json({
            message: `Error happened on server: "${err}"`
          })
      );
};


// adds product to db

exports.updateProduct = (req, res) => {
  Product.findById(req.params.id)
      .then(product => {
        if (!product) {
          return res.status(400).json({
            message: `Product with id "${req.params.id}" is not found.`
          });
        } else {
          const productFields = _.cloneDeep(req.body);

          try {
            productFields.name = productFields.name
                .toLowerCase()
                .trim()
                .replace(/\s\s+/g, " ");
          } catch (err) {
            return res.status(400).json({
              message: `Error happened on server: "${err}"`
            });
          }

          const updatedProduct = queryCreator(productFields);

          Product.findOneAndUpdate(
              { _id: req.params.id },
              { $set: updatedProduct },
              { new: true }
          )
              .then(product => res.json(product))
              .catch(err =>
                  res.status(400).json({
                    message: `Error happened on server: "${err}"`
                  })
              );
        }
      })
      .catch(err =>
          res.status(400).json({
            message: `Error happened on server: "${err}"`
          })
      );
};


exports.deleteProduct = async (req, res) => {

  const { _id } = req.body;

  await Product.find({_id: _id}).then(product => {
    if (!product) {
      return res.json({
        message: "Product not found"
      })
    }

    Product.findOneAndDelete({_id: _id}).then(response => {
      return res.json({
        message: "Product deleted successfully",
        deletedProduct: response
      })
    })

  })
}

exports.getProducts = async (req, res, next) => {

  const mongooseQuery = filterParser(req.query);

  const perPage = Number(req.query.perPage);
  const startPage = Number(req.query.startPage);
  const sort = req.query.sort;
  const q = typeof req.query.q === 'string' ? req.query.q.trim() : null

  if (q) {
    mongooseQuery.name = {
      $regex: new RegExp(q, "i"),
    };
  }

  try {
    const products = await Product.find(mongooseQuery)
        .skip(startPage * perPage - perPage)
        .limit(perPage)
        .sort(sort)

    const total = await Product.countDocuments(mongooseQuery);

    res.json({ data: products, total });
  } catch (err) {
    res.status(400).json({
      message: `Error happened on server: "${err}" `
    });
  }
};

exports.getProductById = async (req, res) => {

  if (req === {} && typeof req === "object"){
    return res.json({message: "Incorrect request, please, provide data"});
  }

  const { startPage, perPage, sort, name, category, day, random, pid } = req.query;
  const { id, color, size } = req.body;

  if (pid){

    await Product.find({id: pid}).then(product => {
      if (product){
        return res.json(product);
      } else return res.json({
        message: "Product not found."
      })
    })
  }

  if (id){

    try {
      if (color && size){
        const pipeline = [
          {$match: { id: id}},
          {
            $set: {
              products: {
                $map: {
                  input: "$products",
                  as: "product",
                  in: {
                    color: "$$product.color",
                    image: "$$product.image",
                    sizes: {
                      $filter: {
                        input: "$$product.sizes",
                        as: "size",
                        cond: { $eq: ["$$size.size", size.toUpperCase()] }
                      }
                    }
                  }
                }
              }
            }
          },
          {
            $addFields: {
              products: color ? {
                $filter: {
                  input: "$products",
                  as: "product",
                  cond: { $eq: ["$$product.color", color] }
                }
              } : "$products"
            }
          },
        ]

        const product = await Product.aggregate(pipeline).exec();

        return res.json(product);
      }

      else if (color && !size){
        const pipeline = [
          {$match: { id: id}},
          {
            $addFields: {
              products: {
                $map: {
                  input: "$products",
                  as: "product",
                  in: {
                    color: "$$product.color",
                    image: "$$product.image",
                    sizes: "$$product.sizes"
                  }
                }
              }
            }
          },
          {
            $set: {
              products: {
                $filter: {
                  input: "$products",
                  as: "product",
                  cond: { $eq: ["$$product.color", color] }
                }
              }
            }
          },
        ]

        const product = await Product.aggregate(pipeline).exec();
        return res.json(product);
      }

      else if (size && !color){
        const pipeline = [
          {$match: { id: id}},
          {
            $set: {
              products: {
                $map: {
                  input: "$products",
                  as: "product",
                  in: {
                    color: "$$product.color",
                    image: "$$product.image",
                    sizes: {
                      $filter: {
                        input: "$$product.sizes",
                        as: "size",
                        cond: { $eq: ["$$size.size", size.toUpperCase()] }
                      }
                    }
                  }
                }
              }
            }
          },
        ]

        const product = await Product.aggregate(pipeline).exec();
        return res.json(product);

      }

      else {
        const product = await Product.find({id: id}).exec();
        return res.json(product);
      }

    } catch (e) {
      return res.status(400).json(e);
    }

  }

  else if (random){
    const products = await Product.aggregate([{
      $sample: {size: 3}
    }]).exec();
    return res.json(products)
  }

  else if (startPage && perPage || sort || name || category || day){

    const query = req.query;
    const newQuery = {};
    for (let [key, value] of Object.entries(query)) {
      newQuery[key] = value.split(',');
    }

    const constructFilterQuery = (filters) => {

      const query = {};

      if (filters.new && filters.new.length === 1) {
        if (filters.new[0] === 'true') {
          query.new = true
        } else if (filters.new[0] === 'false') {
          query.new = false
        }
      }

      if (filters.day && filters.day.length > 0){
        query.day = {$in: filters.day.map(item => new RegExp(item, 'i'))}
      }

      if (filters.fabric && filters.fabric.length > 0) {
        query.fabric = {$in: filters.fabric.map(fab => new RegExp(fab, 'i'))};
      }

      if (filters.color && filters.color.length > 0) {
        query["products.color"] = {$in: filters.color.map(col => new RegExp(col, 'i'))};
      }

      if (filters.type && filters.type.length > 0) {
        query.type = {$in: filters.type.map(typ => new RegExp(typ, 'i'))};
      }

      if (filters.category && filters.category.length > 0) {
        query.category = {$in: filters.category.map(cat => new RegExp(cat, 'i'))};
      }

      if (filters.size && filters.size.length > 0) {
        query["products.sizes.size"] = {$in: filters.size.map(sz => new RegExp(sz, 'i'))};
      }

      if (filters.plusSize && filters.plusSize.length > 0){
        query.plusSize = true;
      }

      return query;

    };

    const getProducts = async (filters, startPage, perPage) => {

      try {

        const query = constructFilterQuery(filters);
        let skip, limit;
        if (isNaN(startPage) && isNaN(perPage)) {
          skip = 0;
          limit = 10;
        } else {
          skip = (startPage - 1) * perPage
          limit = perPage;
        }

        if (filters.sort) {

          const newSort = filters.sort[0];

          if (newSort === 'bestSeller') {

            if (filters.size) {

              const colors = filters.color ? filters.color : null;
              const pipeline = [

                { $match: query },
                {
                  $addFields: {
                    products: colors.length > 0 ? {
                      $filter: {
                        input: "$products",
                        as: "product",
                        cond: {
                          $in: ["$$product.color", colors]
                        }
                      }
                    } : "$products"
                  }
                },
                {
                  $addFields: {
                    totalQuantity: {
                      $sum: {
                        $map: {
                          input: "$products",
                          as: "product",
                          in: {
                            $sum: "$$product.sizes.quantity"
                          }
                        }
                      }
                    }
                  }
                },
                {
                  $set: {
                    products: {
                      $map: {
                        input: "$products",
                        as: "product",
                        in: {
                          color: "$$product.color",
                          image: "$$product.image",
                          sizes: {
                            $filter: {
                              input: "$$product.sizes",
                              as: "size",
                              cond: {
                                $eq: ["$$size.size", filters.size[0].toUpperCase()]
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                },
                {
                  $sort: {
                    totalQuantity: 1
                  }
                },
                { $skip: skip },
                { $limit: limit }
              ];

              const products = await Product.aggregate(pipeline)
                  .exec();
              return res.json(products);
            }

            if (!filters.size) {


              const color = filters.color ? filters.color : null;

              const pipeline = [
                { $match: query },
                {
                  $addFields: {
                    totalQuantity: {
                      $sum: {
                        $map: {
                          input: "$products",
                          as: "product",
                          in: {
                            $sum: "$$product.sizes.quantity"
                          }
                        }
                      }
                    }
                  }
                },
                {
                  $addFields: {
                    products: filters.color && filters.color.length > 0 ? {
                      $filter: {
                        input: "$products",
                        as: "product",
                        cond: {
                          $in: ["$$product.color", filters.color]
                        }
                      }
                    } : "$products"
                  }
                },
                {
                  $sort: {
                    totalQuantity: 1
                  }
                },
                { $skip: skip },
                { $limit: limit }

              ]

              const products = await Product.aggregate(pipeline)
                  .exec();
              return res.json(products);

            }
          }

          if (newSort === '-price') {

            if (filters.size) {

              const color = filters.color ? filters.color : null;

              const pipeline = [

                { $match: query },
                {
                  $addFields: {
                    products: filters.color && filters.color.length > 0 ? {
                      $filter: {
                        input: "$products",
                        as: "product",
                        cond: {
                          $in: ["$$product.color", filters.color]
                        }
                      }
                    } : "$products"
                  }
                },
                {
                  $set: {
                    products: {
                      $map: {
                        input: "$products",
                        as: "product",
                        in: {
                          color: "$$product.color",
                          image: "$$product.image",
                          sizes: {
                            $filter: {
                              input: "$$product.sizes",
                              as: "size",
                              cond: {
                                $eq: ["$$size.size", filters.size[0].toUpperCase()]
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                },
                {
                  $sort: {
                    price: 1,
                  }
                },
                { $skip: skip },
                { $limit: limit }

              ]

              const products = await Product.aggregate(pipeline)
                  .exec();

              return res.json(products)

            }
            if (!filters.size) {

              const pipeline = [

                { $match: query },
                {
                  $addFields: {
                    products: filters.color && filters.color.length > 0 ? {
                      $filter: {
                        input: "$products",
                        as: "product",
                        cond: {
                          $in: ["$$product.color", filters.color]
                        }
                      }
                    } : "$products"
                  }
                },
                { $sort: { price: 1 } },
                { $skip: skip },
                { $limit: limit }

              ]

              const products = await Product.aggregate(pipeline).exec();

              return res.json(products);

            }

          }

          if (newSort === ' price' || newSort === '+price') {

            if (filters.size) {

              const pipeline = [

                { $match: query },
                {
                  $addFields: {
                    products: filters.color && filters.color.length > 0 ? {
                      $filter: {
                        input: "$products",
                        as: "product",
                        cond: {
                          $in: ["$$product.color", filters.color]
                        }
                      }
                    } : "$products"
                  }
                },
                {
                  $set: {
                    products: {
                      $map: {
                        input: "$products",
                        as: "product",
                        in: {
                          color: "$$product.color",
                          image: "$$product.image",
                          sizes: {
                            $filter: {
                              input: "$$product.sizes",
                              as: "size",
                              cond: { $eq: ["$$size.size", filters.size[0].toUpperCase()] }
                            }
                          }
                        }
                      }
                    }
                  }
                },
                {
                  $sort: {
                    price: -1,
                  }
                },
                { $skip: skip },
                { $limit: limit }

              ]

              const products = await Product.aggregate(pipeline)
                  .exec();
              return res.json(products);
            }

            if (!filters.size) {
              const pipeline = [

                { $match: query },
                {
                  $addFields: {
                    products: filters.color && filters.color.length > 0 ? {
                      $filter: {
                        input: "$products",
                        as: "product",
                        cond: {
                          $in: ["$$product.color", filters.color]
                        }
                      }
                    } : "$products"
                  }
                },
                { $sort: { price: -1 } },
                { $skip: skip },
                { $limit: limit }

              ]

              const products = await Product.aggregate(pipeline).exec();
              return res.json(products);

            }

          }

        } else {

          if (filters.size) {

            const pipeline = [

              { $match: query },
              {
                $addFields: {
                  products: filters.color && filters.color.length > 0 ? {
                    $filter: {
                      input: "$products",
                      as: "product",
                      cond: {
                        $in: ["$$product.color", filters.color]
                      }
                    }
                  } : "$products"
                }
              },
              {
                $set: {
                  products: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        color: "$$product.color",
                        image: "$$product.image",
                        sizes: {
                          $filter: {
                            input: "$$product.sizes",
                            as: "size",
                            cond: { $eq: ["$$size.size", filters.size[0].toUpperCase()] }
                          }
                        }
                      }
                    }
                  }
                }
              },
              { $skip: skip },
              { $limit: limit }

            ]

            const products = await Product.aggregate(pipeline)
                .exec();
            return res.json(products);

          }

          if (!filters.size) {

            const pipeline = [

              { $match: query },
              {
                $addFields: {
                  products: filters.color && filters.color.length > 0 ? {
                    $filter: {
                      input: "$products",
                      as: "product",
                      cond: {
                        $in: ["$$product.color", filters.color]
                      }
                    }
                  } : "$products"
                }
              },
              { $skip: skip },
              { $limit: limit }

            ]
            const products = await Product.aggregate(pipeline).exec();
            return res.json(products);
          }

        }

      } catch (e) {
        return res.status(400).json(e);
      }

    }

    await getProducts(newQuery, parseInt(newQuery.startPage), parseInt(newQuery.perPage))

  }
}
