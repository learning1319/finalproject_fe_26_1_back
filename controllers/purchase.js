const Purchase = require('../models/Purchase')
const Stripe = require('stripe');
const uniqueRandom = require("unique-random");
const {ObjectId} = require("mongodb");
const stripe = Stripe(process.env.STRIPE_SECRET_KEY);
const rand = uniqueRandom(10000000, 99999999);

// если добавлять детали оплаты, адреса и метода доставки и товаров тогда нужно
// обьект с данными метода оплаты
// обьект с данными метода доставки
// обьект с данными адреса (хотя адрес должен быть в методе доставки)
// массив продуктов (оттуда же и извлечем все цены)

exports.addPurchase = async (req, res) => {

    const { userId , name, email, paymentDetails, shippingDetails, products, totalPrice } = req.body;

    console.log(shippingDetails)
    const newPurchase = new Purchase({
        userId: userId,
        purchaseNo: rand(),
        name: name,
        email: email,
        paymentDetails: paymentDetails,
        shippingDetails: shippingDetails,
        products: products,
        totalPrice: totalPrice,
    });

    await newPurchase.save()

    try {

        const paymentIntent = await stripe.paymentIntents.create({
            amount: totalPrice * 100,
            currency: 'usd',
            payment_method_types: ['card'],
            confirmation_method: 'automatic',
            confirm: false,
        });

        res.json({ clientSecret: paymentIntent.client_secret });

    } catch (error) {
        res.json({ error: error.message });
    }
}

exports.getUserPurchases = (req, res) => {

    const { userId } = req.query;

    Purchase.find({ userId: userId })
        .then(purchase => res.json(purchase))
        .catch(e => res.json({
            message: `Error happened on server: ${e}`
        }));
}

// searches for purchase by user id and
exports.updatePurchase = async (req, res) => {

    const { _id, status } = req.body;

    await Purchase.findOne({_id: new ObjectId(_id)}).then(purchase => {
        if (status){
            purchase.status = status;
        }
        purchase.save().then(updatedPurchase => res.json(updatedPurchase))
           .catch(err => res.json({
                message: `Error happened on server: ${err}`
            }));
    })
}

exports.getPurchases = async (req, res) => {
    await Purchase.find().then(result => {
        return res.json(result);
    });
}