const Address = require('../models/Address');
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(10000000, 99999999);

// example of address to add
// {
//     "userId":"152",
//     "email":"vl@gmail.com",
//     "firstName":"Vladislav",
//     "lastName":"Ivanov",
//     "country":"Ukraine",
//     "city":"Kyiv",
//     "addressLine1":"Hryhorenka 03a",
//     "addressLine2":"04",
//     "zip":"00-111",
//     "phone":"0244524421"
// }

exports.addAddress = async (req, res) => {
    const { userId, country, city, addressLine1, addressLine2, zip } = req.body;

    const initialQuery = _.cloneDeep(req.body);
    initialQuery.addressNo = rand();

    try {
        const defaultAddress = await Address.findOne({
            userId: userId,
            default: true
        });

        if (defaultAddress) {
            return res.status(400).json({ message: "Default address already exists for this user" });
        }

        const existingAddress = await Address.findOne({
            userId: userId,
            country: country,
            city: city,
            addressLine1: addressLine1,
            addressLine2: addressLine2,
            zip: zip,
        });

        if (existingAddress) {
            return res.status(400).json({ message: "Address already exists" });
        }

        const newAddress = new Address(queryCreator(initialQuery));
        const savedAddress = await newAddress.save();

        return res.status(200).json({
            message: "Added new address",
            details: savedAddress
        });

    } catch (error) {
        return res.status(500).json({
            message: 'Internal server error',
            error: error.message
        });
    }
};

// {userId: userId}, all addresses of user
exports.getAddresses = async (req, res) => {

    const { userId } = req.body;

    await Address.find({
        userId: userId
    }).then(result => {
        return res.json(result);
    }).catch(e => { res.json({
        message: `Error happened getting products`,
        error: e.message
    })})

}


exports.getAddressById = async (req, res) => {

    const { userId, addressLine1, addressLine2 } = req.body;
    await Address.find({
        userId: userId,
        addressLine1: addressLine1,
        addressLine2: addressLine2
    }).then(resp => {
        res.status(200).json(resp)
    })
        .catch(e => res.status(500).json({
            message: `Error happened finding product by address`,
            error: e.message
        }))
}

exports.updateAddress = async (req, res) => {

    const { userId, newAddress }  = req.body;

    try {
        const updatedAddress = await Address.findOneAndUpdate(
            {
                userId: userId,
                default: true
            },
            newAddress,
            { new: true, runValidators: true }
        );

        if (!updatedAddress){
            return res.status(400).json({
                message: `No address for updating was found`
            })
        }

        return res.json(updatedAddress);

    } catch (e){
        return res.status(500).json({
            message: 'Error happened while updating address',
            error: e.message
        })
    }
}

exports.deleteAddress = async (req, res) => {
    const { userId } = req.body;
    try {
        const result = await Address.deleteOne({
            userId: userId,
            default: true,
        });
        if (result.deletedCount === 0) {
            return res.status(404).json({
                message: `No address found`
            });
        }
        return res.json({
            message: `Address was deleted`
        });
    } catch (error) {
        return res.status(500).json({
            message: `Error happened deleting address`,
            error: error.message
        });
    }
};

// finds default address to be shown
exports.getDefaultAddress = async (req, res) => {
    const { userId } = req.query;
    await Address.findOne({
        userId: userId,
        default: true
    }).then(address => {
        if (!address){
            return res.json({
                message: "No default address was found"
            })
        }
        return res.json(address);
    })
}