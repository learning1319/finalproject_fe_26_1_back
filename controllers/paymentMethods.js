const PaymentMethod = require("../models/PaymentMethod");
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(10000000, 99999999);


// {userID, paymentDetails: {}}
exports.addDefaultPaymentMethod = async (req, res) => {

    const { paymentDetails, userId } = req.body;

      PaymentMethod.findOne({ userId: userId}).then(paymentMethod => {
          if (paymentMethod) {

              return res.status(200).json({
                  message: `Payment Method ${paymentDetails} already exists`
              });

          } else {

              const data = _.cloneDeep(req.body);
              data.paymentNo = rand();
              const newPaymentMethod = new PaymentMethod(queryCreator(data));

              newPaymentMethod
                  .save()
                  .then(paymentMethod => res.status(200).json(paymentMethod))
                  .catch(err =>
                      res.status(400).json({
                          message: `Error happened adding payment method`,
                          error: err.message
                      })
                  );
          }
      });
};


// {userId, paymentDetails, newPaymentDetails}
exports.updatePaymentMethod = (req, res, next) => {

    const { newPaymentDetails, userId } = req.body;
    const match = { userId: userId, default: true };

  PaymentMethod.findOne(match)
    .then(paymentMethod => {

      if (!paymentMethod) {

          return res.status(400).json({
          message: `Payment Method was not found.`
        });

      } else {

        PaymentMethod.findOneAndUpdate(
          match,
          {paymentDetails: newPaymentDetails},
          { new: true }
        )
          .then(paymentMethod => res.json(paymentMethod))
          .catch(err =>
            res.status(400).json({
              message: `Error happened when updating payment method`,
                error: err.message
            })
          );
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

// {userId, paymentDetails}

exports.deletePaymentMethod = (req, res, next) => {

    const { userId } = req.body;
    const match = { userId: userId, default: true}

  PaymentMethod.findOne(match).then(
    async paymentMethod => {
      if (!paymentMethod) {
        return res.status(400).json({
          message: `Payment Method was not added.`
        });
      } else {

          PaymentMethod.findOneAndDelete(match)
              .then(dM => res.status(200).json({
                  message: `Payment Method was deleted"`,
                  deletedPayment: dM
              }))
              .catch(e => res.status(400).json({
                  massage: `Error happened deleting payment method`,
                  error: e.message
              }))
      }
    }
  );
};

// {userId}
exports.getPaymentMethods = (req, res, next) => {

    const { userId } = req.body;

  PaymentMethod.find({userId: userId})
    .then(paymentMethods => res.status(200).json(paymentMethods))
    .catch(err =>
      res.status(400).json({
        message: `Error happened getting all payment methods`,
          error: err.message
      })
    );
};

exports.getUserMethod = async (req, res) => {
    const { userId } = req.query;
    await PaymentMethod.findOne({
        userId: userId,
        default: true
    }).then(method => {
        if (!method){
            return res.json({
                method: "Method was not found"
            })
        }
        return res.json(method);
    })
}

exports.addPaymentMethod = async (req, res) => {

    const { userId, paymentDetails} = req.body;
    const { cardNumber, cardCVV, cardExpiryDate } = paymentDetails;

    await PaymentMethod.findOne({userId: userId, cardNumber: cardNumber, cardCVV: cardCVV, cardExpiryDate: cardExpiryDate})
        .then(method => {
            if (!method){
                method.save().then(response => {
                    return res.json({
                        message: "New payment method was added"
                    })
                })
            }
        })
}